# vue-cli-plugin-sync-mocks

:truck: 实现离线调试后台接口：通过代理缓存接口响应数据到本地，并开启本地文件代理服务。

1. 支持 webpack 插件， vue-cli 插件和 函数等模式

## 安装

首先你需要先全局安装 `@vue/cli` 。

然后在你的项目根目录执行以下命令：

```bash
vue add sync-mocks
```

## 配置

1. 在项目根目录下的文件 `package.json 中 添加 syncMockOptions`；
   或者
2. 在项目根目录下的文件 `vue.config.js` （如果没有请创建）中添加：

```javascript
module.exports = {
  devServer: {
    proxy: {
      // ...在这里设置你的代理配置
    }
  },
  pluginOptions: {
    // 该插件对应的配置
    syncMockOptions: {
      headers: {
        cookie: ''
      },
      mode: 2,
      dir: [
        './mock'
      ]
  }
}
```

## 配置项说明

| 配置名       | 类型                              | 描述                                                           |
| ------------ | --------------------------------- | -------------------------------------------------------------- | ----- | ---------------------------------------------------------------- |
| type         | Number                            | 2: 写入和读取， 1: 读取， 0：写入（默认）                      |
| pathHandler  | Function：(url, opts)             | 文件路径拦截器                                                 |
| headers      | Object                            | 代理头部                                                       |
| increment    | Number                            | 增量限制： 当字符长度比历史长度大 increment 时更新， 默认：100 |
| interceptor  | Function： (data, filePath, type) | 数据拦截器                                                     |
| cache        | Boolean                           | 是否启用缓存 默认 true                                         |
| mode         | Number                            | mode: 0: 取消覆盖 1: 全量覆盖 2: 增量覆盖（默认） 3: 新加数据  |
| dir          | String                            | Function                                                       | Array | 离线缓存文件存放地址。默认值：`"node_modules/.cache/sync-mock/"` |
| syncMockData | Boolean                           | 是否同步数据到 mock 本地 类型 type=2， 0                       | Array | 离线缓存文件存放地址。默认值：`"node_modules/.cache/sync-mock/"` |

注意:

该插件是通过代理实现离线缓存的，所以如果你没有配置任何代理，则不会生效。
syncMockData
