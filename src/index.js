// type Opts = {
//     type: 0 | 1, // 读写
//     mode:   1 | 2 | 3 | 4, // mode: 0: 取消覆盖 1: 全量覆盖 2: 增量覆盖 3: 新加数据,4 函数过滤待开发
//     dir: Array<string> | string
// }

const mock = require('./mock.js');
const getDir = function (options) {
    options = options || {};
    let dir = options.dir || './node_modules/.cache/sync-mock';
    let mode = options.mode == null ? 2 : options.mode;
    if (typeof dir === 'function') {
        dir = options.dir();
    }
    if (!dir || !dir.length) return [];
    if (typeof dir === 'string') {
        return [
            {
                ...options,
                dir,
                mode,
            },
        ];
    }
    if (Array.isArray(dir)) {
        return dir.map((item) => {
            return typeof item === 'string'
                ? {
                      ...options,
                      dir: item,
                      mode,
                  }
                : {
                      ...options,
                      ...item,
                      dir: item.dir,
                      mode: item.mode == null ? mode : item.mode,
                  };
        });
    }
    if (typeof dir === 'object') {
        return [
            {
                ...options,
                ...dir,
                mode: dir.mode == null ? mode : dir.mode,
            },
        ];
    }
    return [];
};
const setProxyConfig = function (dir, devServer, rwType) {
    if (!dir || !dir.length || !devServer || !devServer.proxy) return;
    let proxy = devServer.proxy;
    Object.keys(proxy).forEach((key) => {
        let value = proxy[key];
        if (typeof value === 'string') {
            proxy[key] = {
                target: proxy[key],
            };
        }
        // 读取代理
        if (rwType == 1) {
            const isHttp = dir.some((item) => {
                return item.dir === '$http';
            });
            proxy[key].bypass = function (req, res) {
                if (!new RegExp('^' + key).test(req.path)) return;
                let is$Http = false;
                let isEnd = dir.some((item) => {
                    if (item.dir === '$http') {
                        is$Http = true;
                        return true;
                    }
                    return mock.readMockData(req, res, item);
                });
                // 走代理网络
                if (is$Http) {
                    req.is$Http = true;
                    return;
                }
                // 本地没有mock数据
                if (!isEnd) {
                    mock.setHeaders(res);
                    res.end(JSON.stringify({ code: 0, data: null, message: '未获取接口数据' }), 'utf8');
                    console.warn(`未获取接口数据：${req.path}`);
                }
                // 取消请求
                return false;
            };
            if (isHttp) {
                // 这是读取
                proxy[key].selfHandleResponse = true;
                proxy[key].onProxyRes = async function (ProxyRes, req, res) {
                    mock.setHeaders(res, ProxyRes);
                    if (!req.is$Http) return ProxyRes.pipe(res);
                    const data = await mock.getBody(ProxyRes);
                    if (!data?.length) {
                        const afterDir = dir.slice(dir.indexOf('$http') + 1);
                        const isEnd = afterDir.some((item) => {
                            if (item.dir === '$http') {
                                return false;
                            }
                            return mock.readMockData(req, res, item);
                        });
                        if (!isEnd) {
                            res.end(JSON.stringify({ code: 0, data: null, message: '未获取接口数据' }), 'utf8');
                            console.warn(`未获取接口数据：${req.path}`);
                        }
                    } else {
                        res.end(data, 'utf8');
                        dir.forEach((item) => {
                            if (item.dir === '$http' || !item.syncMockData) return;
                            mock.writeData(data, req, null, {
                                ...item,
                                mode: 3,
                                type: 0,
                            }).catch((err) => console.warn(err));
                        });
                    }
                };
            }
            return;
        }
        proxy[key].bypass = function (req, res) {
            try {
                const item = dir[0] || {};
                Object.keys(item.headers || {}).forEach((key) => {
                    let value = item.headers[key].replace(/[\u4E00-\u9FA5]+/g, '');
                    req.headers[key] = value;
                    if (/(set-)?cookie$/i.test(key)) {
                        let cookies = (value || '').split(';').map((item) => {
                            return item.trim() + ';Path=/;Max-Age=36000;';
                        });
                        let setCookie = res.getHeader('set-cookie') || [];
                        res.setHeader('Set-Cookie', [...setCookie, ...cookies]);
                    }
                    res.setHeader(key, value);
                });
            } catch (error) {}
            return;
        };
        // 启动拦截响应输出
        proxy[key].selfHandleResponse = rwType == 2;
        if (rwType == 2) {
            proxy[key].onError = function (err, req, res) {
                let isEnd =
                    new RegExp('^' + key).test(req.path) &&
                    dir.some((item) => {
                        if (item.dir === '$http') return;
                        return mock.readMockData(req, res, {
                            ...item,
                            type: 1,
                        });
                    });
                if (!isEnd) {
                    mock.setHeaders(res);
                    res.end(JSON.stringify({ code: 0, data: null, message: '未获取接口数据' }));
                    console.warn(`未获取接口数据：${req.path}`);
                }
            };
        }
        // 写入代理
        proxy[key].onProxyRes = function (ProxyRes, req, res) {
            dir.forEach((item) => {
                if (item.dir === '$http') return;
                mock.writeMockData(ProxyRes, req, res, item).catch((err) => console.warn(err));
            });
        };
    });
    devServer.proxy = proxy;
    return proxy;
};
function SyncMockPlugin(apiOrOptions, proxyOptions) {
    if (process.env.NODE_ENV === 'production') return;
    apiOrOptions = apiOrOptions || {};
    process.isStopVueCli = process.isStopVueCli || apiOrOptions.isStopVueCli;
    // 是webpack 配置插件
    if (this instanceof SyncMockPlugin) {
        this.options = apiOrOptions;
        this.dir = getDir(this.options);
        return;
    }
    // 是手动函数调用 返回proxy
    if (!apiOrOptions.configureWebpack && proxyOptions) {
        let dir = getDir(apiOrOptions);
        return setProxyConfig(dir, { proxy: proxyOptions }, apiOrOptions.type) || proxyOptions;
    }
    // 是vue-cli 插件
    if (apiOrOptions.configureWebpack && !process.isStopVueCli) {
        let syncMockOptions =
            proxyOptions.devServer &&
            ((proxyOptions.pluginOptions && proxyOptions.pluginOptions.syncMockOptions) || process.VUE_CLI_SERVICE.pkg.syncMockOptions || {});
        let dir = getDir(syncMockOptions);
        if (dir.length) {
            apiOrOptions.configureWebpack(() => {
                let proxy = setProxyConfig(dir, proxyOptions.devServer, syncMockOptions.type);
                if (proxy) proxyOptions.devServer.proxy = proxy;
            });
        }
    }
}

SyncMockPlugin.prototype.apply = function (compiler) {
    if (process.env.NODE_ENV === 'production') return;
    setProxyConfig(this.dir, compiler.options.devServer, this.options.type);
};

module.exports = SyncMockPlugin;
