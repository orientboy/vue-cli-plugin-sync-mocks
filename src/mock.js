const fs = require('fs');
const path = require('path');
const glob = require('glob');
/**
 * 从proxyRes获取body数据，返回json对象
 * @param {*} proxyRes
 */
function getBody(proxyRes) {
    return new Promise((resolve) => {
        let body = [];
        proxyRes.on('error', function () {
            resolve();
        });
        proxyRes.on('end', function () {
            body = Buffer.concat(body).toString();
            resolve(body);
        });
        proxyRes.on('data', function (chunk) {
            body.push(chunk);
        });
    });
}
exports.getBody = getBody;
const CacheMockFiles = {};
// 备份数据
async function writeData(data, req, ProxyRes, options = { mode: 2 }) {
    const { mode, interceptor, dir, type, cache = true, increment = 200, pathHandler, syncMockData } = options;
    const api = req.path.replace(/\?[\W\w]*$/gi, '');
    const rwType = type == 2;
    if (!api || !mode || mode < 1) {
        return;
    }
    const pathApi = typeof pathHandler === 'function' ? pathHandler(api, options) : api;
    const dirPath = path.join(dir, pathApi);
    const filePath = path.join(dirPath, '/index.json');
    const hasFile = (cache && CacheMockFiles[filePath]) || fs.existsSync(filePath);
    // 新加数据, 已经有直接返回
    if (mode == 3 && hasFile) {
        return;
    }
    const res = ProxyRes && req.res;
    const length = data?.length || 0;
    // 兼容接口和本地数据
    const bool =
        !length &&
        res &&
        rwType &&
        readMockData(req, res, {
            ...options,
            type: 1,
        });
    if (bool) return;
    if (type == 2 && res) {
        setHeaders(res, ProxyRes);
        res.end(data, 'utf8');
    }
    if (!(syncMockData || type == 2 || type == 0)) return;
    try {
        data = JSON.parse(data || '{}');
    } catch (error) {
        // 不支持其他类型
        data = null;
    }
    let result = typeof interceptor === 'function' ? interceptor(data, filePath, type) : !(!data || +data.code !== 0 || +data.code !== 200);
    if (result === false) return;
    if (result && typeof result === 'object') data = result;
    data.api = api;
    data.length = length;
    if (hasFile) {
        CacheMockFiles[dirPath] = true;
        try {
            const localData = JSON.parse(fs.readFileSync(filePath, 'utf8') || 'null') || {};
            // MockJs 直接返回
            if (localData?.MockJs) return;
            // 增量覆盖
            if (mode == 2 && localData?.length >= data.length - (increment || 0)) return;
        } catch (error) {
            console.error(error);
            return;
        }
    } else {
        fs.mkdirSync(dirPath, { recursive: true });
    }
    // 全量覆盖 mode: 0
    fs.writeFile(
        filePath,
        JSON.stringify(data, null, 4),
        {
            encoding: 'utf8',
            flag: 'w+',
        },
        function (err) {
            if (err) {
                console.error(err);
                return;
            }
            CacheMockFiles[dirPath] = true;
            console.log(`写入mock数据成功: ${dirPath}`);
        }
    );
}

exports.writeData = writeData;
/**
 * @param {*} req
 * @param {*} res
 * @param {*} options mode: 0: 取消覆盖 1: 全量覆盖 2: 增量覆盖 3: 新加数据,4 函数过滤待开发
 */
async function writeMockData(ProxyRes, req, res, options = { mode: 2 }) {
    const { mode, dir, type, cache = true, pathHandler } = options;
    const api = req.path.replace(/\?[\W\w]*$/gi, '');
    const rwType = type == 2;
    if (!api || !mode || mode < 1) {
        return rwType && ProxyRes.pipe(res);
    }
    const pathApi = typeof pathHandler === 'function' ? pathHandler(api, options) : api;
    const dirPath = path.join(dir, pathApi);
    const filePath = path.join(dirPath, '/index.json');
    const hasFile = (cache && CacheMockFiles[filePath]) || fs.existsSync(filePath);
    // 新加数据, 已经有直接返回
    if (mode == 3 && hasFile) {
        CacheMockFiles[dirPath] = true;
        return rwType && ProxyRes.pipe(res);
    }
    const data = await getBody(ProxyRes);
    writeData(data, req, ProxyRes, options);
}

exports.writeMockData = writeMockData;

const CacheMockData = {};
const getFileJS = function (dir, api, req) {
    let data = null;
    const method = req.method.toUpperCase();
    const files = glob.globSync(path.join(process.cwd(), dir, './**/*.js').replace(/[\\/]+/gi, '/'));
    files.some((url) => {
        delete require.cache[url];
        const result = require(url);
        const key = Object.keys(result).find((key) => {
            key = key.replace(/\s+/g, ' ');
            if (key.indexOf('/:') > -1) {
                const reg = new RegExp(key.replace(/\//g, '\\/').replace(/\/:[^/]+/g, '/[^/]+'));
                return reg.test(api) || reg.test(`${method} ${api}`);
            }
            return key === api || key === `${method} ${api}`;
        });
        if (key) {
            data = result[key];
            return true;
        }
        return false;
    });
    return data;
};
/**
 * @param {*} req
 * @param {*} res
 * @param {*} options
 */
function readMockData(req, res, options) {
    const { dir, cache = true, interceptor, type, pathHandler } = options;
    const api = req.path.replace(/\?[\W\w]*$/gi, '');
    const pathApi = typeof pathHandler === 'function' ? pathHandler(api, options) : api;
    const filePath = path.join(path.join(dir, pathApi), '/index.json');
    const jsData = CacheMockData[filePath] || getFileJS(dir, api, req);
    if (jsData || fs.existsSync(filePath)) {
        let data = jsData || fs.readFileSync(filePath, 'utf8');
        if (typeof data === 'function') {
            if (cache) CacheMockData[filePath] = jsData;
            setHeaders(res);
            data(req, res);
            console.log(`读取mock-js数据成功`);
            return true;
        }
        try {
            if (typeof data !== 'string') {
                data = JSON.stringify(data);
            }
            if (typeof interceptor === 'function') {
                const result = interceptor(JSON.parse(data), filePath, type);
                if (result) {
                    data = typeof result !== 'string' ? JSON.stringify(result) : result;
                }
            }
        } catch (error) {}
        if (cache) CacheMockData[filePath] = data;
        setHeaders(res);
        res.end(data, 'utf8');
        console.log(`读取mock数据成功: ${filePath}`);
        return true;
    }
    return false;
}
exports.readMockData = readMockData;

function setHeaders(res, ProxyRes) {
    const headers = (ProxyRes ? ProxyRes.headers : res.req?.headers) || {};
    Object.keys(headers).forEach((key) => {
        if (headers[key] && key !== 'content-Type' && key !== 'Content-Type' && key !== 'content-length') {
            const key1 = key.replace(/^\w|-\w/gi, function (match) {
                if (match[0] === '-') {
                    return '-' + match[1].toUpperCase();
                }
                return match.toUpperCase();
            });
            res.setHeader(key1, headers[key]);
        }
    });
    res.setHeader('Content-Type', (ProxyRes && headers['content-Type']) || res.getHeader('Content-Type') || 'application/json;charset=utf-8');
}

exports.setHeaders = setHeaders;
